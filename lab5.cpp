/*
  Kyle Racel
  kbracel
  Lab 5
  Lab Section: 4
  TA: Anurata Hridi
*/

//include statements
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

//fuction prototypes
string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}

int main(int argc, char const *argv[]) {
  srand(unsigned (time(0)));

  //a full deck of cards is created here
  struct Card deck[52];
  struct Card temp;
  int k = 0;
  for(int i = 0; i < 4; i++)
  {
    for(int j = 2; j < 15; j++, k++)
    {
      switch(i)
      {
        case 0:
        temp.suit = SPADES;
        break;
        case 1:
        temp.suit = HEARTS;
        break;
        case 2:
        temp.suit = DIAMONDS;
        break;
        case 3:
        temp.suit = CLUBS;
        break;
      }
      temp.value = j;
      deck[k] = temp;
    }
  }

  //this shuffles the deck of cards
  random_shuffle(&deck[0], &deck[51]+1, myrandom);

  //this creates a hands from the first 5 cards of the shuffled deck
  struct Card hand[5];
  for(int i = 0; i < 5; i++)
  {
    hand[i] = deck[i];
  }

  //this sorts the random hand of cards
  sort(&hand[0], &hand[4]+1, suit_order);

  //this prints the sorted hand of cards
  for(int i = 0; i < 5; i++)
  {
    cout << right << setw(10) << get_card_name(hand[i]) << " of "
      << get_suit_code(hand[i]) << endl;
  }

  return 0;
}

//this function takes in two cards and returns true or false
//to help the sorting fuction decide if the cards should be switched
bool suit_order(const Card& lhs, const Card& rhs) {
  if(lhs.suit < rhs.suit)
  {
    return true;
  }
  else if(lhs.suit == rhs.suit)
  {
    if(lhs.value < rhs.value)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    return false;
  }
}

//this returns the symbol for the suit of a card
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

//this returns the card name for a card
string get_card_name(Card& c) {
  switch (c.value)
  {
    case 2:   return "2";
    case 3:   return "3";
    case 4:   return "4";
    case 5:   return "5";
    case 6:   return "6";
    case 7:   return "7";
    case 8:   return "8";
    case 9:   return "9";
    case 10:  return "10";
    case 11:  return "Jack";
    case 12:  return "Queen";
    case 13:  return "King";
    case 14:  return "Ace";
    default:  return "";
  }
}
